// ros and friends
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
// project-specific
#include <agile_grasp/FindGrasps.h>
#include <agile_grasp/grasp_localizer.h>

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;

PointCloud::Ptr g_cloud(new PointCloud);
ros::Publisher g_handles_rviz_pub, g_pcl_pub;
std::string g_base_frame;
std::string g_sensor_frame;
std::string g_cloud_topic;
bool g_hasCloud = false;
bool g_applyStaticSensorToHeadOffset;
ros::NodeHandle* g_node;

Localization* g_localization;
std::string g_svm_file_name;
int g_min_inliers;
bool g_plots_handles;

agile_grasp::Grasp createGraspMsg(const GraspHypothesis& hand, int handleIndex)
{
  agile_grasp::Grasp msg;
  tf::vectorEigenToMsg(hand.getGraspBottom(), msg.center);
  tf::vectorEigenToMsg(hand.getAxis(), msg.axis);
  tf::vectorEigenToMsg(hand.getApproach(), msg.approach);
  tf::vectorEigenToMsg(hand.getGraspSurface(), msg.surface_center);
  msg.width.data = hand.getGraspWidth();
  msg.handleIndex = handleIndex;
  return msg;
}

agile_grasp::Grasps createGraspsMsgFromHands(const std::vector<Handle>& handles)
{
  agile_grasp::Grasps msg;  
  for (int i = 0; i < handles.size(); i++)
  {
    const std::vector<GraspHypothesis>& hands = handles[i].getHandList();
    const std::vector<int>& inliers = handles[i].getInliers();
    for (int j = 0; j < inliers.size(); j++)
      msg.grasps.push_back(createGraspMsg(hands[inliers[j]], i));
  }
  msg.header.stamp = ros::Time::now();
  std::cout << "Created grasps msg containing " << msg.grasps.size() << " hands\n";
  return msg;
}

void cloudCallback(const sensor_msgs::PointCloud2ConstPtr& msg)
{
  // check whether point cloud is empty
  if (msg->height == 0 || msg->width == 0)
    return;
  
  // update the sensor frame name
  g_sensor_frame = msg->header.frame_id;
  
  // convert sensor_msg to PCL point cloud
  pcl::fromROSMsg(*msg, *g_cloud);
  
  // done
  ROS_INFO("Received cloud with %d points.", (int)g_cloud->size());
  g_hasCloud = true;
}

bool graspsCallback(agile_grasp::FindGrasps::Request& req, agile_grasp::FindGrasps::Response& resp)
{
  // get cloud
  g_hasCloud = false;
  ros::Subscriber cloudSub = g_node->subscribe(g_cloud_topic, 10, cloudCallback);
  ros::Rate waitCloudRate(10);
  while (!g_hasCloud && ros::ok())
    ros::spinOnce();
    waitCloudRate.sleep();
  cloudSub.shutdown();
  if (!ros::ok()) return false;
  
  // create identity transform
  tf::StampedTransform identity_transform(tf::Transform(tf::Quaternion(0,0,0,1)), ros::Time::now(), "", "");
 
  // lookup transform from sensor frame to base frame
  tf::StampedTransform transform, transform_base_to_head;
  tf::TransformListener transform_listener;    
  
  if (g_applyStaticSensorToHeadOffset)
  {
    ROS_INFO("getting transform with static offset %s -> %s", g_base_frame.c_str(), g_sensor_frame.c_str());
    
    transform_listener.waitForTransform(g_base_frame, "/r2/head", ros::Time(0), ros::Duration(5.0));
    transform_listener.lookupTransform(g_base_frame, "/r2/head", ros::Time(0), transform_base_to_head);
    
    tf::StampedTransform transform_head_to_asus;
    transform_head_to_asus.setOrigin(tf::Vector3(0.0897354, 0.122035, -0.00753185));
    transform_head_to_asus.setRotation(tf::Quaternion(0.707105, -0.0010822, 0.707107, 0.0010796));
    
    tf::StampedTransform transform_base_to_asus;
    transform_base_to_asus.mult(transform_base_to_head, transform_head_to_asus);
    transform = transform_base_to_asus;
  }
  else
  {
    ROS_INFO("looking up transform %s -> %s", g_base_frame.c_str(), g_sensor_frame.c_str());
    transform_listener.waitForTransform(g_base_frame, g_sensor_frame, ros::Time(0), ros::Duration(5.0));
    transform_listener.lookupTransform(g_base_frame, g_sensor_frame, ros::Time(0), transform);
  }
  
  std::cout << "Cloud Transform (xyz): " << transform.getOrigin().x() << " " << transform.getOrigin().y() << " " << transform.getOrigin().z() << "\n";
  std::cout << "Cloud Transform (xyzw): " << transform.getRotation().getX() << " " << transform.getRotation().getY() << " " << transform.getRotation().getZ() << " " << transform.getRotation().getW() << "\n";
  
  // transform point cloud to base frame
  pcl_ros::transformPointCloud(*g_cloud, *g_cloud, transform);
  
  // find grasps
  std::vector<int> indices(0);
  std::vector<GraspHypothesis> hands = g_localization->localizeHands(g_cloud, g_cloud->size(), indices, false, false);
  std::vector<GraspHypothesis> antipodal_hands = g_localization->predictAntipodalHands(hands, g_svm_file_name);
  std::vector<Handle> handles = g_localization->findHandles(antipodal_hands, g_min_inliers, 0.005, g_plots_handles);
  
  resp.grasps_msg = createGraspsMsgFromHands(handles);
  resp.grasps_msg.header.frame_id = g_base_frame;
  
  // visualize point cloud
  sensor_msgs::PointCloud2 pc2msg;
  pcl::toROSMsg(*g_cloud, pc2msg);
  pc2msg.header.stamp = ros::Time::now();
  pc2msg.header.frame_id = g_base_frame;
  g_pcl_pub.publish(pc2msg);
  
  return true;
}

int main(int argc, char **argv)
{
  // create node
  ros::init(argc, argv, "grasps_server");
  g_node = new ros::NodeHandle("~");
  
  // create publishers and services
  ros::ServiceServer handlesService = g_node->advertiseService("/grasps_server", graspsCallback);
  g_pcl_pub = g_node->advertise<sensor_msgs::PointCloud2>("cloud", 10);
  
  // read parameters
  GraspLocalizer::Parameters params;
  
  std::vector<double> workspace;
  std::vector<double> camera_pose;
  double taubin_radius;
  double hand_radius;
  
  g_node->getParam("cloud_topic", g_cloud_topic);
  g_node->getParam("svm_file_name", g_svm_file_name);
  g_node->getParam("num_threads", params.num_threads_);
  g_node->getParam("num_samples", params.num_samples_); 
  g_node->getParam("num_clouds", params.num_clouds_);
  g_node->getParam("finger_width", params.finger_width_);
  g_node->getParam("hand_outer_diameter", params.hand_outer_diameter_);
  g_node->getParam("hand_depth", params.hand_depth_);
  g_node->getParam("init_bite", params.init_bite_);
  g_node->getParam("hand_height", params.hand_height_);
  g_node->getParam("min_inliers", params.min_inliers_);
  g_node->getParam("workspace", workspace);
  g_node->getParam("camera_pose", camera_pose);
  g_node->getParam("plots", params.plots_hands_);
  
  Eigen::Matrix4d R;
  for (int i=0; i < R.rows(); i++)
    R.row(i) << camera_pose[i*R.cols()], camera_pose[i*R.cols() + 1], camera_pose[i*R.cols() + 2], camera_pose[i*R.cols() + 3];
  
  Eigen::VectorXd ws(6);
  ws << workspace[0], workspace[1], workspace[2], workspace[3], workspace[4], workspace[5];
  params.workspace_ = ws;
  
  g_min_inliers = params.min_inliers_;
  g_plots_handles = params.plots_hands_;
  
  g_node->getParam("apply_static_sensor_to_head_offset", g_applyStaticSensorToHeadOffset);
  g_node->getParam("base_frame", g_base_frame);
  
  // setup
  
  g_localization = new Localization(params.num_threads_, true, params.plots_hands_);
  g_localization->setCameraTransforms(params.cam_tf_left_, params.cam_tf_right_);
  g_localization->setWorkspace(params.workspace_);
  g_localization->setNumSamples(params.num_samples_);
  g_localization->setNeighborhoodRadiusTaubin(taubin_radius);
  g_localization->setNeighborhoodRadiusHands(hand_radius);
  g_localization->setFingerWidth(params.finger_width_);
  g_localization->setHandOuterDiameter(params.hand_outer_diameter_);
  g_localization->setHandDepth(params.hand_depth_);
  g_localization->setInitBite(params.init_bite_);
  g_localization->setHandHeight(params.hand_height_);
  
  // start execution
  ROS_INFO("Ready to find grasps.");
  ros::spin();
  
  return 0;
}
