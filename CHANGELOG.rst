^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package agile_grasp
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


0.7.1 (2015-05-16)
------------------
* added Rviz visualization
* 0.7.0
* update CHANGELOG
* Merge branch 'master' into hydro
* adjust CMakeLists.txt and package.xml for ROS Hydro
* added dependency for visualization msgs
* fixed package name in launch files
* changed version number
* 0.6.3
* removed screenshots folder
* hydro version
* Contributors: Andreas, baxter

* 0.7.0
* update CHANGELOG
* Merge branch 'master' into hydro
* adjust CMakeLists.txt and package.xml for ROS Hydro
* added dependency for visualization msgs
* fixed package name in launch files
* changed version number
* 0.6.3
* removed screenshots folder
* hydro version
* Contributors: Andreas, baxter

0.7.0 (2015-05-16)
------------------
* Merge branch 'master' into hydro
* adjust CMakeLists.txt and package.xml for ROS Hydro
* added dependency for visualization msgs
* fixed package name in launch files
* changed version number
* 0.6.3
* removed screenshots folder
* hydro version
* Contributors: Andreas, baxter

0.6.2 (2015-04-11)
------------------
* fix some dependencies
* Contributors: atp

0.6.1 (2015-04-10)
------------------
* initial commit
* Contributors: atp
